import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Apikey from '../constants/Apikey';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import localStorage from '@react-native-async-storage/async-storage';
import RenderHtml from 'react-native-render-html';
import { WebView } from 'react-native-webview';

function DetailNews({navigation: {navigate}, route}) {
  const {news} = route.params;
  const [refreshing, setRefreshing] = useState(false);

  function handleRefresh() {
    setRefreshing(true);
  }

  return (
    <View style={styles.container}>
      <SafeAreaView style={{flex: 1}}>
        {/* <RenderHtml
          style={styles.isiNews}
          contentWidth={wp('100%')}
          source={{html: news.url}}
          enableExperimentalMarginCollapsing={true}
        /> */}
        <WebView source={{ uri: `${news.url}` }} />
      </SafeAreaView>
    </View>
  );
}

export default DetailNews;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: hp('100%'),
    width: wp('100%'),
  },
  containerJudul: {
    height: hp('10%'),
    width: wp('100%'),
  },
  containerIsi: {
    height: hp('10%'),
    width: wp('100%'),
  },
  judul: {
    fontSize: hp('2.5%'),
    fontFamily: 'Roboto-Bold',
    textAlign: 'justify',
  },
  isiNews: {
    height: hp('100%'),
  },
  imageNews: {
    width: wp('100%'),
    height: hp('75%'),
  },
});
