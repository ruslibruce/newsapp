import {
  StyleSheet,
  Text,
  View,
  FlatList,
  RefreshControl,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import Apikey from '../constants/Apikey';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import localStorage from '@react-native-async-storage/async-storage';

function Home({navigation: {navigate}}) {
  const [news, setNews] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    // Update the document title using the browser API
    getNews();
  }, []);

  useEffect(() => {
    // Update the document title using the browser API
    if (refreshing) {
      getNews();
    }
  }, [refreshing]);

  async function getNews() {
    try {
      let response = await axios({
        method: 'get',
        url: `https://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=${Apikey}`,
      });
      const data = response.data;
      console.log(JSON.stringify(data));
      setNews(data.articles);
      setRefreshing(false);
    } catch (error) {
      console.log(error);
    }
  }

  function handleRefresh() {
    setRefreshing(true);
  }

  return (
    <View style={styles.container}>
      <SafeAreaView style={{flex: 1}}>
        <FlatList
          keyExtractor={(item, index) => `${index}`}
          data={news}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={handleRefresh}
              colors={['#529F45', '#FFFFFF']}
            />
          }
          onEndReachedThreshold={0.5}
          initialNumToRender={10}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          pagingEnabled={true}
          renderItem={({item, index}) => {
            return (
              <TouchableOpacity
                key={index}
                onPress={async () => {
                  navigate('DetailNews', {
                    news: item,
                  });
                }}>
                <View style={styles.containerJudul}>
                  <Text style={styles.judul}>{item.title}</Text>
                </View>
                <View
                  style={{
                    borderBottomWidth: 1,
                    width: wp('100%'),
                    borderColor: '#BCBCBC',
                    marginVertical: wp('2%'),
                  }}
                />
                <View style={styles.containerIsi}>
                  <Text style={styles.isiNews}>{item.description}</Text>
                </View>
                <Image
                  resizeMode="cover"
                  style={styles.imageNews}
                  source={{uri: item.urlToImage}}
                />
              </TouchableOpacity>
            );
          }}
        />
      </SafeAreaView>
    </View>
  );
}

export default Home;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: hp('100%'),
    width: wp('100%'),
  },
  containerJudul: {
    height: hp('10%'),
    width: wp('100%'),
  },
  containerIsi: {
    height: hp('10%'),
    width: wp('100%'),
  },
  judul: {
    fontSize: hp('2.5%'),
    fontFamily: 'Roboto-Bold',
    textAlign: 'justify',
  },
  isiNews: {
    fontSize: hp('2%'),
    fontFamily: 'Roboto-Regular',
  },
  imageNews: {
    width: wp('100%'),
    height: hp('75%'),
  },
});
